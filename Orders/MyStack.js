import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from './component/LoginScreen/Login';
import { Image } from 'react-native'

const Stack = createNativeStackNavigator();

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SettingScreen from './component/SettingScreen/Setting';
import HomeScreen from './component/HomeScreen/Home';
import DetailsScreen from './component/DetailsScreen/Details';

const Tab = createBottomTabNavigator();

import { createDrawerNavigator } from '@react-navigation/drawer';
const Drawer = createDrawerNavigator();

//add menu
function DrawerLayout() {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Home" component={HomeScreen} options={{ title: 'Home' }} />
            <Drawer.Screen name="Setting" component={SettingScreen} options={{ title: 'Setting' }} />
        </Drawer.Navigator>

    );
}

//add tabs
const MyTabs = ({navigation,route}) => {
    return (
        <Tab.Navigator screenOptions={{ headerShown: false }} initialRouteName ='Home'>
             <Tab.Screen name="Home" children={() => <HomeScreen navigation ={navigation} route={route}/>} options={{
                title: 'trang chủ',
                tabBarIcon: () => (<Image source={require('./component/image/icons_home.png')} style={{ width: 30, height: 30 }} resizeMethod='resize' />)
            }} />
            <Tab.Screen name="Setting" children={() => <SettingScreen {...props} />} options={{
                title: 'Cài đặt',
                tabBarIcon: () => (<Image source={require('./component/image/icons_home.png')} style={{ width: 30, height: 30 }} resizeMethod='resize' />)
            }}
            />
        </Tab.Navigator>
    );
}

const MyStack = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName='Login'>
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{ title: 'Login' }}
                />
                <Stack.Screen name="HomeTabs"
                    component={MyTabs}
                    options={{ title: 'MyTabs', headerShown: false }} />

                <Stack.Screen name='details' component={DetailsScreen}
                  options={{ title: 'Details' }}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default MyStack;



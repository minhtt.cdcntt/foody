import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    Contain: {
        flex: 1,
        paddingHorizontal: 40,
        alignItems: 'center'
    },
    input: {
        marginTop: 10,
        height: 45,
        backgroundColor: '#fff5',
        paddingHorizontal: 10,
        borderRadius: 15,
        borderColor: 'red',
        borderWidth: 1,
        width: '100%',
    },
    borderLogin: {
        marginTop: 20,
        height: 45,
        width: '50%',
        backgroundColor: 'blue',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textLogin: {
        color: 'white',
    },
    viewRegister: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    txtRegister: {
        marginTop: 20,
        marginHorizontal: 10,
        color: 'blue',
    }
})

export default styles;
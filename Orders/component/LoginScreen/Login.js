import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, TouchableOpacity, Image, Alert, KeyboardAvoidingView, Platform } from 'react-native';
import styles from './Style';
import { createDrawerNavigator } from '@react-navigation/drawer';
const Drawer = createDrawerNavigator();
const Login = ({ navigation }) => {

    const [username, setUser] = useState('');
    const [password, setPass] = useState('');

    const handleLogin = () => {
        if (username.length == 0)
            Alert.alert('vui long nhap tai khoan', [{
                text: 'dong'
            }])
        else if (password.length == 0)
            Alert.alert('vui long nhap mat khau', [{
                text: 'dong'
            }])
        else if (username != '1234' || password != '1234') {
            Alert.alert('sai tai khoan hoac mat khau', [{
                text: 'dong'
            }])
        }
        else {
            navigation.replace('HomeTabs',
                {
                    setUser,
                    user: username,
                    pass: password,
                    obj:{
                        name:'Tran Thanh Minh',
                        class:'lop 12',
                    }
                });
        }
    }

    return (

        <View style={styles.Contain}>
            <Image style={{ height: 200, resizeMode: 'contain' }} source={require('../image/bg_login.png')} />
            <TextInput placeholder='nhap tai khoan' text ={username} style={styles.input} onChangeText={text => setUser(text)} />
            <TextInput placeholder='nhap mat khau' secureTextEntry={true} style={styles.input} onChangeText={text => setPass(text)} />
            <TouchableOpacity style={styles.borderLogin} onPress={handleLogin}>
                <Text style={styles.textLogin}>Dang nhap</Text>
            </TouchableOpacity>

            <View style={styles.viewRegister}>
                <TouchableOpacity>
                    <Text style={styles.txtRegister}>Quen mat khau</Text>
                </TouchableOpacity>

                <TouchableOpacity>
                    <Text style={styles.txtRegister}>Dang ky moi</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Login;

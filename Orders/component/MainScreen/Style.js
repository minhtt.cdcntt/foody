import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    borderitem:{
        backgroundColor: 'blue',
        padding: 20
    },
    container2: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 16
      },
      navigationContainer: {
        backgroundColor: "#ecf0f1"
      },
      paragraph: {
        padding: 16,
        fontSize: 15,
        textAlign: "center"
      }
});

export default styles;
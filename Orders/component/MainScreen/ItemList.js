import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import style from '../MainScreen/Style'

const Item = (props) => {
    return (
        <TouchableOpacity onPress={()=>props.showItem()}>
            <View style={style.borderitem}>
                <Text>{props.id}</Text>
                <Text>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default Item;
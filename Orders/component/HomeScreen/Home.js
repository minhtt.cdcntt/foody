import React from 'react';
import { View, Text, TouchableOpacity,Alert } from 'react-native'

const Home = ({navigation,route}) => {
    const toDetail = () =>{
        navigation.navigate('details')
     //  Alert.alert('minh');
    }
    console.log('minh');
    console.log(route.params);
   // const { user}  = route.params;

   const logOut = () =>{
    navigation.replace('Login')
   }

    return (
        <View style={{ marginTop: 50 }}>
            <TouchableOpacity onPress={toDetail}>
                <Text>Chi tiết {route.params.user}</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={logOut}>
                <Text>Đăng xuất </Text>
            </TouchableOpacity>
        </View>
    )
}

export default Home;